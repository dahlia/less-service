#!/usr/bin/env python
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


setup(name='less-service',
      py_modules=['less'],
      version='0.1.0',
      url='http://less-service.herokuapp.com/',
      author='Hong Minhee',
      author_email='minhee' '@' 'dahlia.kr',
      description='The client library for LESS-Compiler-as-a-Service.',
      zip_safe=True,
      install_requires=['six'],
      tests_require=['Attest'],
      test_loader='attest:auto_reporter.test_loader',
      test_suite='test.suite',
      classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.5',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.0',
        'Programming Language :: Python :: 3.1',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: Jython',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Programming Language :: Python :: Implementation :: Stackless'
      ],
      license='MIT License')

