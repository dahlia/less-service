/*jshint node: true */

"use strict";

var less = require('less'),
    http = require('http'),
    url = require('url'),
    path = require('path'),
    redis = require('redis-url').connect(process.env.REDISTOGO_URL),
    fs = require('fs'),
    markdown = require('markdown');

function makeUrl(req, path) {
  var host = req.headers.host || 'localhost' + (port ? ':' + port : '');
  var base = 'http://' + host;
  if (path.charAt(0) == '/') return base + path;
  return base + '/' + path;
}

function makeKey(length) {
  if (typeof length == 'undefined') {
    length = 32;
  }
  var chars = '0123456789abcdefghijklmnopqrstuvwxyz';
  var result = '';
  while (result.length < length) {
    result += chars.charAt(Math.floor(Math.random() * chars.length));
  }
  return result;
}

var routingTable = [];

var httpd = http.createServer(function(req, res) {
  var pathMatched = false;
  var parsed = url.parse(req.url, true);
  req.query = parsed.query;
  req.on('data', function(chunk) {
    if (typeof req.body == 'undefined') {
      req.body = chunk;
    } else {
      chunk.copy(req.body, req.body.length);
    }
  });
  var candi = null;
  for (var i = 0; i < routingTable.length; ++i) {
    var method = routingTable[i][0];
    var pathRegexp = routingTable[i][1];
    var func = routingTable[i][2];
    var match = pathRegexp.exec(parsed.pathname);
    if (!match) continue;
    pathMatched = true;
    var args = match.slice(1);
    if (method != req.method) {
      if (method == 'GET' && req.method == 'HEAD') {
        candi = [func, args];
      }
      continue;
    }
    return func.apply(httpd, [req, res].concat(args));
  }
  if (candi !== null) {
    var end = res.end;
    res.end = function() {
      end.apply(res);
    };
    return candi[0].apply(httpd, [req, res].concat(candi[1]));
  } else if (pathMatched) {
    res.writeHead(405, {'Content-Type': 'text/plain'});
    res.end('Method Not Allowed');
    return;
  }
  res.writeHead(404, {'Content-Type': 'text/plain'});
  res.end('Page Not Found');
});

routingTable.push(['GET', /^\/$/, function(req, res) {
  fs.readFile('README.md', 'utf-8', function(err, data) {
    if (err) {
      res.writeHead(500, {'Content-Type': 'text/plain'});
      res.end('Internal Server Error');
    } else {
      var readme = markdown.parse(data);
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.write('<!DOCTYPE html>\n');
      res.write('<html><head><meta charset="utf-8"><title>LESS-Service');
      res.write('</title>\n<style type="text/css">\n');
      fs.readFile('readme.less', 'utf-8', function(err, lessStyle) {
        less.render(lessStyle, function(err, css) {
          res.write(css);
          res.write('\n</style></head><body>');
          res.write(readme);
          res.end('</body></html>');
        });
      });
    }
  });
}]);

routingTable.push(['POST', /^\/$/, function(req, res) {
  var key = makeKey();
  var url = makeUrl(req, '/' + key);
  redis.hset([key, '__key__', key], function() {
    redis.expire([key, 3600], function() {
      res.writeHead(201, {Location: url, 'Content-Type': 'application/json'});
      res.end(JSON.stringify({url: url}, undefined, 2));
    });
  });
}]);

routingTable.push([
  'GET',/^\/([0-9a-z]{32})\/?$/,
  function(req, res, key) {
    redis.hget([key, '__key__'], function(err, reply) {
      if (err || reply != key) {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.end('null');
      } else {
        var method = req.query.get && req.query.get == 'number' ? 'hlen'
                                                                : 'hkeys';
        redis[method](key, function(err, replies) {
          var result;
          if (method == 'hlen') {
            result = replies - 1;
          } else {
            result = {};
            for (var i = 0; i < replies.length; ++i) {
              if (replies[i] == '__key__') continue;
              result[replies[i]] = makeUrl(req, '/' + key + '/' + replies[i]);
            }
          }
          res.writeHead(200, {'Content-Type': 'application/json'});
          res.end(JSON.stringify(result, undefined, 2));
        });
      }
    });
  }
]);

routingTable.push(['DELETE', /^\/([0-9a-z]{32})\/?$/, function(req, res, key) {
  redis.hget([key, '__key__'], function(err, reply) {
    if (err || reply != key) {
      res.writeHead(404, {'Content-Type': 'application/json'});
      res.end('null');
    } else {
      redis.del(key, function(err, reply) {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end('null');
      });
    }
  });
}]);

routingTable.push([
  'HEAD', /^\/([0-9a-z]{32})\/(.+)$/,
  function(req, res, key, filename) {
    redis.hget([key, '__key__'], function(err, reply) {
      if (err || reply != key) {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.end('null');
      } else {
        redis.hexists([key, filename], function(err, reply) {
          res.writeHead(reply ? 200 : 404,
                        {'Content-Type': 'application/json'});
          res.end();
        });
      }
    });
  }
]);

routingTable.push([
  'GET',/^\/([0-9a-z]{32})\/(.+)$/,
  function(req, res, key, filename) {
    var compress = ['true', 't', '1', 'yes', 'y'].indexOf(
      (req.query.compress || '').toLowerCase()
    ) >= 0;
    redis.hmget([key, '__key__', filename], function(err, replies) {
      if (err || replies[0] != key || replies[1] === null) {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.end('null');
      } else {
        less.Parser.importer = function(file, paths, callback) {
          if (paths.indexOf('') < 0) {
            paths.unshift('');
          }
          var files = paths.map(function(p) { return path.join(p, file); });
          redis.hmget([key].concat(files), function(err, replies) {
            for (var i = 0; i < replies.length; ++i) {
              if (replies[i] === null) continue;
              var parser = new less.Parser({
                  paths: [path.dirname(files[i])].concat(paths),
                  filename: files[i]
              });
              parser.parse(replies[i], function(e, root) {
                callback(e, root, replies[i]);
              });
              return;
            }
            res.writeHead(500, {'Content-Type': 'application/json'});
            res.end(JSON.stringify({
              error: 'Internal Server Error',
              description: 'Dependencies are not satisfied',
              missingFile: file,
              paths: paths
            }, undefined, 2));
          });
        };
        try {
          var parser = less.Parser({
            filename: filename
          });
          parser.parse(replies[1], function(err, root) {
            res.writeHead(200, {'Content-Type': 'text/css'});
            res.end(root.toCSS({ compress: compress }));
          });
        } catch (e) {
          res.writeHead(500, {'Content-Type': 'application/json'});
          res.end(JSON.stringify({
            error: 'Internal Server Error',
            detail: e
          }, undefined, 2));
        }
      }
    });
  }
]);

routingTable.push([
  'PUT', /^\/([0-9a-z]{32})\/(.+)$/,
  function(req, res, key, filename) {
    redis.hget([key, '__key__'], function(err, reply) {
      if (err || reply != key) {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.end(JSON.stringify({
          error: 'Page Not Found',
          description: 'Uninitialized key'
        }, undefined, 2));
      } else {
        if (typeof req.body == 'undefined') {
          res.writeHead(400, {'Content-Type': 'application/json'});
          res.end(JSON.stringify({
            error: 'Bad Request',
            description: 'Request body is not present'
          }, undefined, 2));
        } else {
          redis.hset([key, filename, req.body.toString()], function() {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.end('true');
          });
        }
      }
    });
  }
]);

routingTable.push([
  'DELETE', /^\/([0-9a-z]{32})\/(.+)$/,
  function(req, res, key, filename) {
    redis.hget([key, '__key__'], function(err, reply) {
      if (err || reply != key) {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.end('null');
      } else {
        redis.hdel([key, filename], function(err, reply) {
          if (err || !reply) {
            res.writeHead(404, {'Content-Type': 'application/json'});
            res.end('null');
          } else {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.end(JSON.stringify(filename));
          }
        });
      }
    });
  }
]);

var port = process.env.PORT || 3000;
httpd.listen(port, '0.0.0.0', function() {
  console.log('Server running at http://0.0.0.0:' + port + '/');
});

