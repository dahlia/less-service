from attest import Tests
from . import less


suite = Tests()
suite.register(less.tests)


if __name__ == '__main__':
    suite.run()
