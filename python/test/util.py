import re
import os


def resource_path(path):
    path = path.split('/')
    return os.path.join(os.path.abspath(os.path.dirname(__file__)),
                        u'resources', *path)


def load_resource(path):
    with open(resource_path(path)) as f:
        return f.read()


def shrink_whitespace(s):
    return re.sub(r'\s+', ' ', s).strip()

