LESS Compiler-as-a-Service
==========================

<http://less-service.herokuapp.com/>

**LESS-Service** provides the compiler of [LESS][], the superset language that
extends CSS, through HTTP API.  It could be useful for other languages than
JavaScript e.g. Python.  It fully supports complete LESS features including
static `@import` statement.

[LESS]: http://lesscss.org/


Crating a temporary storage
---------------------------

In order to compile your LESS stylesheets, you have to create a temporary
storage first.  (Every storage expires soon.)

    POST / HTTP/1.1
    Host: less-service.herokuapp.com
    Content-Length: 0

If you request the root path (`/`) of the service with `POST` method,
it will return the URL of created temporary storage with `201 Created`:

    HTTP/1.1 201 Created
    Content-Type: application/json
    Location: http://less-service.herokuapp.com/key0string0what0you0just0created
    Content-Length: 80

    {
      "url":"http://less-service.herokuapp.com/key0string0what0you0just0created"
    }


Uploading stylesheet files
--------------------------

You can upload multiple stylesheet files to compile into the storage.  Request
the filename-appended storage URL with `PUT` method.  The request body has to
be a LESS content.  For example, if the URL of just created storage is
`http://less-service.herokuapp.com/key0string0what0you0just0created` and the
filename of LESS stylesheet to compile is `mystyle.less`:

    PUT /key0string0what0you0just0created/mystyle.less HTTP/1.1
    Host: less-service.herokuapp.com
    Content-Type: text/x-less
    Content-Length: 42

    @my-color: red;
    body { color: @my-color; }

And then it will return just `true` which means the success with `200 OK`:

    HTTP/1.1 200 OK
    Content-Type: application/json
    Content-Length: 4

    true


Fetching compiled result
------------------------

To fetch the compiled result of LESS file, request the uploaded file with `GET`.
For example:

    GET /key0string0what0you0just0created/mystyle.less HTTP/1.1
    Host: less-service.herokuapp.com

It will return the compiled result, `text/css` encoded, with `200 OK`:

    HTTP/1.1 200 OK
    Content-Type: text/css
    Content-Length: 26

    body {
      color: #ff0000;
    }

You can pass `?compress=true` option as well:

    GET /key0string0what0you0just0created/mystyle.less?compress=true HTTP/1.1
    Host: less-service.herokuapp.com

It will remove all whitespace characters from the compiled result:

    HTTP/1.1 200 OK
    Content-Type: text/css
    Content-Length: 26

    body{color:#ff0000;}


Resolving dependencies
----------------------

If your LESS code contains static `@import` but it cannot be found, you will
face `500 Internal Server Error`:

    HTTP/1.1 500 Internal Server Error
    Content-Type: application/json

    {
      "error": "Internal Server Error",
      "description": "Dependencies are not satisfied",
      "missingFile": "reset.less",
      "paths": [
        "lib",
        ""
      ]
    }

In this case, you have to provide the `missingFile` also.  The `paths` field
could be helpful to find the `missingFile`.


Listing files inside storage
----------------------------

To list files inside the storage, simply request the storage URL with `GET`
method:

    GET /key0string0what0you0just0created HTTP/1.1
    Host: less-service.herokuapp.com
    Accept: application/json

It returns the map of filename/url items with `200 OK`:

    HTTP/1.1 200 OK
    Content-Type: application/json

    {
      "mystyle.less": "http://less-service.herokuapp.com/.../mystyle.less",
      "lib/reset.lett": "http://less-service.herokuapp.com/.../lib/reset.less"
    }


Expiring storage explicitly
---------------------------

Every storage expires soon automatically, but if you want to expire it
explicitly, simply request the storage URL with `DELETE`:

    DELETE /key0string0what0you0just0created HTTP/1.1
    Host: less-service.herokuapp.com
    Accept: application/json

It will return `null` with `200 OK`:

    HTTP/1.1 200 OK
    Content-Type: application/json

    null


Open source
-----------

The source code of the service is available under [AGPL][].  You can host
the web application by yourself.  Checkout it from the [Git repository][]:

    $ git clone https://bitbucket.org/dahlia/less-service.git

It is written by [Hong Minhee][].

[AGPL]: http://www.gnu.org/licenses/agpl.html
[Git repository]: https://bitbucket.org/dahlia/less-service
[Hong Minhee]: http://dahlia.kr/

