from __future__ import absolute_import
from attest import Tests, assert_hook
import os
from .util import shrink_whitespace, load_resource
import less


SERVICE_DOMAIN = os.environ.get('SERVICE_DOMAIN', less.DEFAULT_DOMAIN)

tests = Tests()


@tests.context
def connect():
    global SERVICE_DOMAIN
    d = less.Directory(domain=SERVICE_DOMAIN)
    with d.connection():
        yield d


@tests.test
def as_is(d):
    source = u'''body { color: #00ff00; }'''
    d['site.less'] = source
    assert source == shrink_whitespace(d['site.less'])


@tests.test
def monolithic(d):
    d['site.less'] = load_resource('less/monolithic.less')
    assert d['site.less'] == load_resource('less/monolithic.css')


@tests.test
def multiple(d):
    d['common.less'] = load_resource('less/common.less')
    d['site.less'] = load_resource('less/site.less')
    assert d['site.less'] == load_resource('less/site.css')


@tests.test
def hierarchical(d):
    d['common/mixin.less'] = load_resource('less/common/mixin.less')
    d['page.less'] = load_resource('less/page.less')
    assert d['page.less'] == load_resource('less/page.css')


@tests.test
def parent(d):
    d['common.less'] = load_resource('less/common.less')
    d['site.less'] = load_resource('less/site.less')
    d['pages/user.less'] = load_resource('less/pages/user.less')
    assert d['pages/user.less'] == load_resource('less/pages/user.css')


if __name__ == '__main__':
    tests.run()
