from __future__ import with_statement
import six
import contextlib
from six.moves import http_client as httplib
if six.PY3:
    from urllib.parse import urlparse
else:
    from urlparse import urlparse

__all__ = 'Unit', 'DEFAULT_DOMAIN'

DEFAULT_DOMAIN = 'less-service.herokuapp.com'


class Directory(object):
    """The compiling unit that abstracts the temporary storage.

    .. sourcecode:: pycon

       >>> d = Directory()
       >>> d  # doctest: +ELLIPSIS
       less.Directory('...')
       >>> Directory(key=d.key)  # doctest: +ELLIPSIS
       less.Directory('...')
       >>> with d.connection():
       ...     d['index.less'] = '@import "partial"; body { .mixin; }'
       ...     d['partial.less'] = '.mixin { color: red; }'
       ...     print(d['index.less'])  # doctest: +NORMALIZE_WHITESPACE
       ...
       .mixin {
         color: red;
       }
       body {
         color: red;
       }

    """

    def __init__(self, key=None, domain=DEFAULT_DOMAIN):
        if key is None:
            self.domain = domain
            with self.connection() as http:
                http.request('POST', '/')
                res = http.getresponse()
                assert res.status == 201, 'unexpected code: ' + str(res.status)
                loc = res.getheader('Location')
                assert loc, 'location header is not present'
            url = urlparse(loc)
            domain = url.netloc
            key = url.path[1:]
        elif not isinstance(key, six.string_types):
            raise TypeError('key must be a string, not ' + repr(key))
        self.key = key
        self.domain = domain

    @contextlib.contextmanager
    def connection(self):
        conn = getattr(self, '_conn', None)
        if conn:
            yield conn
            return
        self._conn = httplib.HTTPConnection(self.domain)
        yield self._conn
        self._conn.close()
        self._conn = None

    def __setitem__(self, filename, less):
        if not isinstance(filename, six.string_types):
            raise TypeError('key (filename) must be a string, not ' +
                            repr(filename))
        elif not isinstance(less, six.string_types):
            raise TypeError('value (less code) must be a string, not ' +
                            repr(less))
        if isinstance(less, six.text_type):
            less = less.encode('utf-8')
        with self.connection() as http:
            http.request('PUT', '/' + self.key + '/' + filename, less, 
                         {'Content-Type': 'text/x-less',
                          'Content-Length': str(len(less))})
            res = http.getresponse()
            assert res.status == 200, \
                   'unexpected code: ' + str(res.status) + \
                   '\nunexpected body: ' + repr(res.read())
            result = res.read().strip()
            assert result == six.b('true'), 'unexpected body: ' + repr(result)

    def __getitem__(self, filename):
        if not isinstance(filename, six.string_types):
            raise TypeError('key (filename) must be a string, not ' +
                            repr(filename))
        with self.connection() as http:
            http.request('GET', '/' + self.key + '/' + filename,
                         headers={'Accept': 'text/css'})
            res = http.getresponse()
            assert res.status == 200, \
                   'unexpected code: ' + str(res.status) + \
                   '\nunexpected body: ' + repr(res.read())
            return res.read().decode('utf-8')

    def __repr__(self):
        cls = type(self)
        return cls.__module__ + '.' + cls.__name__ + '(' + repr(self.key) + \
               (self.domain != DEFAULT_DOMAIN and ', ' + repr(self.domain) or
                '') + ')'

